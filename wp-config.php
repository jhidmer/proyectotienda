<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'upci');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'sR&.2kpxm2Rq0Vi`fcHn:%X bI}*2Ia-o/<#AxP6Be/5Q_hHR6xE]7V4t$3YQWU]');
define('SECURE_AUTH_KEY', 'G7F(L;d} J#]2,/HA(EhuB^FL[*Sq9R;b7VSl`iTxu6<q^~!d#5laCK~Zm7PwtSr');
define('LOGGED_IN_KEY', 'G4T_KM0MDfPhFv#{*^QQ}y}zYA&2$ww[.&]kSu6BgtPnvZr?vX_v.I0O-2%lEzFj');
define('NONCE_KEY', 'B)gxFE!JmY4H?SK))Rj~ni,K8]>a QuG]|aG_yZJuCFV@h4;rSwJgWEc.pGlgI/g');
define('AUTH_SALT', 'yJzSxk<4ar6(]f]Qo%U+=;Gh10T0vi!&NMQ<zX7k-i+5me$VF9~<<HCaD(wpTL`a');
define('SECURE_AUTH_SALT', '-M}``n/sxi`v0H[zP[?eD-O/Ez[jd7iT^UA@J?+*o# :_|sV1YZ}=)-7.e P7hz4');
define('LOGGED_IN_SALT', 'RtSxP7.z3%>KRl8 sRGtKmOX<-&}o.lPCav?6TvZ_8#~_9_#+28ls;:*dH=c%~=H');
define('NONCE_SALT', 'cic!dQ$EE$yl12q^9*vFQ&Bc+:[t=,7Aj@jN&9Uk,m0g/nwj69SzzB&pH*3kdG0]');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_upci';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

